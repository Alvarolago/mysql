DROP USER IF EXISTS 'administrador'@'localhost';
CREATE USER 'administrador'@'localhost' IDENTIFIED BY 'patata';
GRANT ALL ON *.* TO 'administrador'@'localhost';
FLUSH PRIVILEGES;

DROP USER IF EXISTS 'cliente'@'localhost';
CREATE USER 'cliente'@'localhost' IDENTIFIED BY 'patata';
GRANT UPDATE(Nombre, Apellidos, Domicilio, DNI), INSERT(Nombre, Apellidos, Domicilio, DNI) ON bddclientes.CLIENTE TO 'cliente'@'localhost';
FLUSH PRIVILEGES;

DROP USER IF EXISTS 'gerente'@'localhost';
CREATE USER 'gerente'@'localhost' IDENTIFIED BY 'patata';
GRANT SELECT, UPDATE, DELETE, INSERT, GRANT OPTION ON bddclientes.* TO 'cliente'@'localhost';
FLUSH PRIVILEGES;

DROP USER IF EXISTS 'cajero'@'localhost';
CREATE USER 'cajero'@'localhost' IDENTIFIED BY 'patata';
GRANT SELECT ON bddclientes.* TO 'cliente'@'localhost';
FLUSH PRIVILEGES;

DROP USER IF EXISTS 'comercial'@'localhost';
CREATE USER 'comercial'@'localhost' IDENTIFIED BY 'patata';
GRANT SELECT, INSERT, DELETE, UPDATE ON bddclientes.* TO 'cliente'@'localhost';

DROP USER IF EXISTS 'cliente1'@'localhost';
CREATE USER 'cliente1'@'localhost' IDENTIFIED BY 'patata';
DROP VIEW IF EXISTS CLIENTE1;
CREATE VIEW CLIENTE1 AS SELECT * FROM CLIENTE WHERE NºCliente='1';
SELECT * FROM CLIENTE1;
FLUSH PRIVILEGES;
