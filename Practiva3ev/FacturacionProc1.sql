SOURCE FacturacionTablas.sql;		/*Debemos actualizar las tablas antes de llamar al procedimiento*/


DROP PROCEDURE IF EXISTS Facturar;

DELIMITER | 

CREATE PROCEDURE Facturar ()

  BEGIN

    DECLARE cnt INT;		/*Se va incrementando para el bucle*/
    DECLARE final INT;
    DECLARE cliente INT;

    DECLARE factura INT;
    DECLARE empleado INT;
    DECLARE BaseImponible INT;
    DECLARE BaseImponibleIVA INT;

    DECLARE comision INT;
    DECLARE Total INT;

    SET final = (SELECT COUNT(*) FROM Pedidos);
    SET cnt = 1;		/*Inicializamos la constante(cnt)y factura para que comienze por 1 o 0 respectivamente*/
    SET factura = 0;		

    llamada: LOOP		/*Llamamos al bucle*/


        SET cliente = (SELECT Pedidos.CodigoCliente FROM Pedidos WHERE Pedidos.CodigoPedido = cnt);
        SET BaseImponible = (SELECT SUM(PrecioUnidad * Cantidad) FROM DetallePedidos WHERE DetallePedidos.CodigoPedido = cnt);
	SET BaseImponibleIVA = (BaseImponible * 0.21);
        SET Total = (BaseImponible + BaseImponible) * 0.21;
        SET empleado = (SELECT CodigoEmpleado From Clientes JOIN Empleados ON CodigoEmpleado = CodigoEmpleadoRepVentas WHERE CodigoCliente = cliente);
        SET comision = Total * .05;


	/*Insertamos en la tabla Facturas en caso de que la BAseImponible no sea NULL*/
	IF BaseImponible IS NOT NULL THEN
        INSERT INTO Facturas (CodigoCliente, BaseImponible, IVA, Total, CodigoFactura) VALUES (cliente, BaseImponible, BaseImponibleIVA, Total, factura);
        END IF;

	/*Incrementamos la factura para ir actualizandola*/
        SET factura = factura + 1;

	/*Guardamos en la tabla comisiones la comisión del 5%*/
	IF empleado IS NOT NULL THEN
	IF comision IS NOT NULL THEN
        INSERT INTO Comisiones VALUES (empleado, comision, factura);
	END IF;
	END IF;


	/*Aumentamos la variable para que actue como bucle*/
  SET cnt = cnt + 1;

	/*Condicion de parada*/
  IF cnt <= final
    THEN ITERATE llamada;
  END IF;

  LEAVE llamada;
  END LOOP llamada;

  END;
  |

DELIMITER ;




CALL facturar();
SELECT * FROM Facturas ORDER BY CodigoCliente;
SELECT * FROM Comisiones ORDER BY CodigoEmpleado;
