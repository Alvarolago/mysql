USE Jardineria;

DROP FUNCTION IF EXISTS ultFactura;

delimiter |

CREATE FUNCTION ultFactura() RETURNS VARCHAR(20)
BEGIN
    DECLARE salida VARCHAR(20);

    /*Con el IF y el ELSE creamos una condición en caso de que no exista devuelve 1 por defecto*/
    IF (SELECT COUNT(Facturas.CodigoFactura) FROM Facturas) = 0 THEN SET salida = '1';
    ELSE SET salida := (SELECT Facturas.CodigoFactura FROM Facturas, Pedidos ORDER BY Pedidos.FechaPedido LIMIT 1);
    END IF;


    RETURN salida;
END
|

delimiter ;
