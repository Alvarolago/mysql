USE Jardineria;


DROP PROCEDURE IF EXISTS estadoPedidos;

DELIMITER |
	
	DROP TABLE IF EXISTS EstadoPedidos;
	CREATE TABLE EstadoPedidos (PedidosTotales INT, EntregadoTiempo INT, EntregadoTarde INT, PorcentajeDeEficiencia DECIMAL(10,2));

CREATE PROCEDURE estadoPedidos()
	BEGIN

		/*Declaramos Variables*/
		DECLARE PedidosTotales INT;
		DECLARE EntregadoTiempo INT;
		DECLARE PorcentajeDeEficiencia DECIMAL(10,2);
		DECLARE EntregadoTarde INT;

		/*Para el porcentaje de eficiencia dividimos los paquetes entregados a tiempo entre los totales*/
		SET PedidosTotales = (SELECT COUNT(Pedidos.CodigoCliente) FROM Pedidos);
		SET EntregadoTiempo = (SELECT COUNT(Pedidos.CodigoPedido) FROM Pedidos WHERE FechaEsperada >= FechaEntrega);
		SET EntregadoTarde = (SELECT COUNT(Pedidos.CodigoPedido) FROM Pedidos WHERE FechaEsperada <= FechaEntrega);
		SET PorcentajeDeEficiencia = ((EntregadoTiempo / PedidosTotales) * 100);

		/*Insertamos los datos de las variables en la tabla creada anteriormente*/
		INSERT INTO EstadoPedidos VALUES (PedidosTotales, EntregadoTiempo, EntregadoTarde, PorcentajeDeEficiencia);

	END

	|
DELIMITER ;

CALL estadoPedidos();
SELECT * FROM EstadoPedidos;
