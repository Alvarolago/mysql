USE Jardineria;

DROP TABLE IF EXISTS Facturas;
DROP TABLE IF EXISTS Comisiones;
DROP TABLE IF EXISTS AsientoContable;

DROP PROCEDURE IF EXISTS facturar;

CREATE TABLE Facturas (
	CodigoCliente INTEGER,
	BaseImponible NUMERIC(15,2),
	IVA NUMERIC(15,2),
	Total NUMERIC(15,2),
	CodigoFactura INTEGER,

	FOREIGN KEY(CodigoCliente) REFERENCES Clientes(CodigoCliente)
);
DESC Facturas;

CREATE TABLE Comisiones (
	CodigoEmpleado INTEGER,
	Comision NUMERIC(15,2),
	CodigoFactura INTEGER,

	FOREIGN KEY(CodigoEmpleado) REFERENCES Empleados(CodigoEmpleado)
	/*FOREIGN KEY(CodigoFactura) REFERENCES Comisiones(CodigoFactura)*/

);
DESC Comisiones;

CREATE TABLE AsientoContable (
	CodigoCliente INTEGER,
	DEBE NUMERIC(15,2),
	HABER NUMERIC(15,2),

	FOREIGN KEY(CodigoCliente) REFERENCES Clientes(CodigoCliente)
);
DESC AsientoContable;


