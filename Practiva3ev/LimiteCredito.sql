DROP PROCEDURE IF EXISTS limiteCredito;

USE Jardineria;

DELIMITER |

    DROP TABLE IF EXISTS ActualizacionLimiteCredito;
    /*Creamos la tabla tal y como indica el ejercicio*/
    CREATE TABLE ActualizacionLimiteCredito (Fecha DATE, CodigoClientes INT, Incremento FLOAT);


CREATE PROCEDURE limitecredito(numero_cliente INT)  /*El cliente debe meter el CodigoCliente del cual se quiere actualizar*/

BEGIN

    /*Declaramos Variables*/
	DECLARE CodigoClientes INT;
	DECLARE Incremento FLOAT;
 	DECLARE TotalCredito FLOAT;
	DECLARE Fecha DATE;
	DECLARE TotalCreditoIncrem DECIMAL(15,2);

    /*Insertamos las consulas en las variables correspondientes*/
    SET Fecha = (SELECT CURDATE());
    SET CodigoClientes = (SELECT Clientes.CodigoCliente FROM Clientes WHERE Clientes.CodigoCliente = numero_cliente);
    SET TotalCredito = (SELECT Clientes.LimiteCredito FROM Clientes WHERE Clientes.CodigoCliente = numero_cliente);
    SET Incremento = (TotalCredito * 0.15);
    SET TotalCreditoIncrem = TotalCredito + Incremento;

    /*Creamos un update para actualizar en la BDD de Jardineria, el limiteCredito en un 15%*/
    UPDATE Clientes SET LimiteCredito = TotalCreditoIncrem WHERE CodigoCliente = numero_cliente;
    /*Con el INSERT creamos un registro para saber cual hemos actualizado concretamente*/
    INSERT INTO ActualizacionLimiteCredito VALUES (Fecha, CodigoClientes, Incremento);

END

|
DELIMITER ;
