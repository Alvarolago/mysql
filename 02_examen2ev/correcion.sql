DROP DATABASE IF EXISTS EXAMEN_BBDD;
CREATE DATABASE EXAMEN_BBDD;
USE EXAMEN_BBDD;

CREATE TABLE Actores (
Nombre VARCHAR(20),
Personaje VARCHAR(30),
FechaNacimiento DATE,
Nacionalidad VARCHAR(20),

CONSTRAINT PK_Actores PRIMARY KEY (Nombre));
DESC Actores;

CREATE TABLE Personajes (
Nombre VARCHAR(20),
Raza VARCHAR(20),
GraduacionMilitar ENUM('Capitan','Teniente','Almirante','Oficial'),
NombreActor VARCHAR(20),
NombrePersonajes VARCHAR(30),

CONSTRAINT PK_Personajes PRIMARY KEY(Nombre),
CONSTRAINT FK_Personajes_Actores FOREIGN KEY (NombreActor) REFERENCES Actores(Nombre),
CONSTRAINT FK_Personajes_Personajes FOREIGN KEY (NombrePersonajes) REFERENCES Personajes(Nombre)
);
DESC Personajes;


CREATE TABLE Naves (
Nombre VARCHAR(30) UNIQUE,
Codigo INT UNSIGNED,
NumeroTripulantes INT UNSIGNED, 

CONSTRAINT PK_Codigo PRIMARY KEY (Codigo)
);
DESC Naves;

CREATE TABLE Planetas (
CodigoPlaneta INT,
Nombre VARCHAR(20),
Galaxia VARCHAR(40),
Problema VARCHAR(255),
CodigoNave INT UNSIGNED,

CONSTRAINT PK_Planetas PRIMARY KEY (CodigoPlaneta),
CONSTRAINT FK_CodigoNave_Planeta FOREIGN KEY (CodigoNave) REFERENCES Naves(Codigo)
);
DESC Planetas;


CREATE TABLE Capitulos (
Numero INT UNSIGNED, 
Temporada INT UNSIGNED, 
Titulo VARCHAR(50) UNIQUE,
Orden INT UNSIGNED,
CodigoPlaneta INT,

CONSTRAINT PK_Capitulos PRIMARY KEY (Numero, Temporada),
CONSTRAINT FK_CODIGOPLANETA_CAPITULO FOREIGN KEY(CodigoPlaneta) REFERENCES Planetas(CodigoPlaneta)
);
DESC Capitulos;

CREATE TABLE AparicionesSeries (
NombrePersonaje VARCHAR(20),
NumeroCapitulo INT UNSIGNED,
NumeroTemporada INT UNSIGNED,

CONSTRAINT FK_AP_Nombre_PERSONAJE FOREIGN KEY (NombrePersonaje) REFERENCES Personajes(Nombre),
CONSTRAINT FK_AP_NUM_CAPITULO FOREIGN KEY (NumeroCapitulo, NumeroTemporada) REFERENCES Capitulos(Numero, Temporada)
);
DESC AparicionesSeries;

CREATE TABLE Peliculas (
AñoLanzamiento YEAR,
Titulo VARCHAR(30),
Director VARCHAR(50),

CONSTRAINT PK_Peliculas PRIMARY KEY (Titulo)
);
DESC Peliculas;

CREATE TABLE AparicionesPelis (
NombrePersonaje VARCHAR(30),
TituloPelicula VARCHAR(30),
Protagonista ENUM('s','n'),

CONSTRAINT FK_APP_NOMBRE_PERSONAJE FOREIGN KEY (NombrePersonaje) REFERENCES Personajes(Nombre),
CONSTRAINT FK_APP_PELIS_PELIS FOREIGN KEY (TituloPelicula) REFERENCES Peliculas(Titulo)
);
DESC AparicionesPelis;

/* modif */ 

ALTER TABLE Capitulos DROP FOREIGN KEY FK_CODIGOPLANETA_CAPITULO;
ALTER TABLE Planetas MODIFY CodigoPlaneta INT UNSIGNED AUTO_INCREMENT; 
ALTER TABLE Capitulos ADD CONSTRAINT FK_CODIGOPLANETA_CAPITULO FOREIGN KEY(CodigoPlaneta) REFERENCES Planetas(CodigoPlaneta);

/* foreign key cascade */ 

ALTER TABLE Capitulos DROP FOREIGN KEY FK_CodigoNave_Planeta;
ALTER TABLE Capitulos ADD CONSTRAINT FK_CodigoNave_Planeta FOREIGN KEY (CodigoNave) REFERENCES Naves(Codigo) ON UPDATE CASCADE;

/* Añadir columna */

ALTER TABLE Peliculas ADD Recaudacion DECIMAL(5,2);

/* hacer insert */

INSERT INTO Actores VALUES ('Luke Perry', 'Spiderman', '1990-01-01', 'Turca'); 
INSERT INTO Actores VALUES ('Galindo', 'Ant-Man', '1930-01-01', 'Marciana');
INSERT INTO Personajes VALUES ('Spiderman', 'Mutante', 'Oficial', 'Lucke Perry', NULL);
INSERT INTO Naves VALUES ('Halcon Milenario', 1, 10);
INSERT INTO Planetas (Nombre, Galaxia, Problema, CodigoNave) VALUES ('Kamino', 'Via Lactea', 'Nos enfrentamos mi capitan, mi capitan', 1);
INSERT INTO Capitulos VALUES (1, 1, 'The beggining Is the End is the beggining', 3, 1);
INSERT INTO Capitulos VALUES (1, 2, '2x01', 1, 1);
INSERT INTO AparicionesSerie VALUES ('Spiderman', 1, 1);
INSERT INTO Peliculas VALUES ('Sin-Chan y las bolas magicas', 2007, 'Nikito NiPongo LaPelota', 123.45); 
INSERT INTO AparicionesPelis VALUES ('Spiderman', 'Nikito NiPongo LaPelota', 's');

/* actualizar */

UPDATE AparicionesSerie SET NumeroTemporada='2' WHERE NumeroCapitulo=1 AND NumeroTemporada=1;

/* borrar */

DELETE FROM Actores WHERE Nombre='Galindo';

/* DROP TABLE Actores; */ 

/* CREAR USUARIO */ 
/*
  CREATE USER 'usuario'@'localhost' IDENTIFIED BY 'usuario';
  GRANT SELECT (COLUMNAS) ON Planetas TO 'usuario'@'localhost';
  REVOKE ALL PRIVILEGIES FROM 'usuario'@'localhost';
*/ 
