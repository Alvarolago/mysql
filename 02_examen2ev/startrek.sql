DROP DATABASE IF EXISTS StarTrek;
CREATE DATABASE StarTrek;
USE StarTrek;

CREATE TABLE Actores (
        Nombre VARCHAR(20),
        Personaje VARCHAR(30),
        FechaNacimiento DATE,
        Nacionalidad VARCHAR(20),

CONSTRAINT PK_Actores PRIMARY KEY(Nombre)
);

CREATE TABLE Personajes (
        Nombre VARCHAR(20),
        Raza VARCHAR(20),
        GraduacionMilitar ENUM('Capitan','Teniente','Almirante','Oficial'),
        NombrePersonajes VARCHAR(30),
        

CONSTRAINT PK_Personajes PRIMARY KEY(Nombre),
CONSTRAINT FK_PersonajesActores FOREIGN KEY(NombreActor) REFERENCES Actores(Nombre),
CONSTRAINT PK_PersonajesPersonajes FOREIGN KEY(NombrePersonajes) REFERENCES Personajes(Nombre)
);

CREATE TABLE Nave (
        Nombre VARCHAR(30),
        Codigo INT UNSIGNED,
        NumTripulantes INT UNSIGNED,

CONSTRAINT PK_Nave PRIMARY KEY(Codigo)
);

CREATE TABLE Planetas (
        Codigo INT UNSIGNED,
        Nombre VARCHAR(30),
        Galaxia VARCHAR(40),
        Problema VARCHAR(255),
        CodigoNave INT UNSIGNED,

CONSTRAINT PK_Planetas PRIMARY KEY(Codigo),
CONSTRAINT FK_PlanetasNave (CodigoNave) REFERENCES Nave(Codigo)
);

CREATE TABLE Capitulo (
        Numero INT UNSIGNED,
        Temporada INT,
        Titulo VARCHAR(50),
        Orden INT UNSIGNED,
        CodigoPlaneta INT UNSIGNED,

CONSTRAINT PK_Capitulo PRIMARY KEY(Numero,Temporada),
CONSTRAINT FK_CapitulosPlanetas FOREIGN KEY(CodigoPlaneta) REFERENCES Planetas(Codigo)
);

CREATE TABLE AparicionesSerie (
        NombrePersonaje VARCHAR(30),
        NombreCapitulo INT UNSIGNED,
        NumeroTemporada INT UNSIGNED,

CONSTRAINT FK_AparicionesSeriePersonaje FOREIGN KEY(NombrePersonaje) REFERENCES Personajes (Nombre),
CONSTRAINT FK_AparicionesSerieCapitulos FOREIGN KEY(NumeroCapitulo, NumeroTemporada) REFERENCES Capitulos(Numero, Temporada));

CREATE TABLE Peliculas (
        Titulo VARCHAR(30),
        AñoLanzamiento YEAR,
        Director VARCHAR(50),

CONSTRAINT PK_Peliculas PRIMARY KEY (Titulo));

CREATE TABLE AparicionesPelis (
        NombrePersonaje VARCHAR(30),
        TituloPelicula VARCHAR(30),
        Protagonista ENUM('Si','No')

CONSTRAINT FK_AparicionesPelisPersonajes FOREIGN KEY 
);

ALTER TABLE Capitulos DROP FOREIGN KEY FK_CapitulosPlanetas;
ALTER TABLE Planetas MODIFY Codigo INT UNSIGNED AUTO_INCREMENT;
ALTER TABLE Planetas ADD CONSTRAINT FK_CapitulosPlanetas FOREIGN KEY(CodigoPlaneta) REFERENCES Planetas(Codigo);

ALTER TABLE Capitulos DROP FOREIGN KEY FK_CapitulosPlanetas;
ALTER TABLE Capitulos ADD CONSTRAINT FK_CapitulosPlanetas FOREIGN KEY(CodigoPlaneta) REFERENCES Planetas(Codigo) ON UPDATE CASCADE;

ALTER TABLE Peliculas ADD Recaudacion DECIMAL(5,2);

INSERT INTO Actores VALUES ('Luke Perry', 'Spiderman', 1990-01-01, 'Turca');
INSERT INTO Personajes VALUES ('Spiderman', 'Mutante', 'Oficial', 'Luke Perry', NULL);
INSERT INTO Naves VALUES ('Halcon Milenario', 1, 10);
INSERT INTO Planetas(Nombre,Galaxia,Problema,CodigoNave) VALUES ('Kamino', 'Via Lactea', 'Nos enfrentamos mi Capitan, Oh mi capitan...', 1);
INSERT INTO Capitulos VALUES (1, 1, 'The being adshijkldsalik', 3, 1);
INSERT INTO Capitulos VALUES (1, 2, '2x01', 3, 1);
INSERT INTO AparicionesSerie VALUES ('Spiderman', 1, 1);
INSERT INTO Peliculas VALUES ('Shin-Chan y las bolas magicas', 2007, 'Nikitop NiPongo', 123.25);
INSERT INTO AparicionesPelis ('Spiderman', 'Shin-Chan y las bolas magicas', 'Si');

UPDATE AparicionesSerie SET NumeroTemporada='2' WHERE NumeroCapitulo=1 AND NumeroTemporada=1;

DELETE FROM Actores WHERE Nombre='Luke Perry'; /*Depende de si el dato está usado se puede borrar o no en este caso no se puede*/ 

/*Tabla Actores no se puede borrar ya que hay muchas foreign keys que dependen de ella */












