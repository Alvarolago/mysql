INSERT INTO CLIENTES VALUES
    ('12345679A', 'Perico', 'de los Palotes', '123456789', 'perico@todo.com'),
    ('11111112Z', 'Fulanito', 'de tal', '123123237', 'Ful@nito.com')
;

INSERT INTO TIENDAS VALUES
    ('Gran Via', 'Madrid', 'Madrid', 'C/Gran Via 32', '123132345', 'Lunes', 'Viernes', 9, 10),
    ('Castellana', 'Madrid', 'Madrid', 'C/Castellana 108', '643254349', 'Lunes', 'Sabado', 8, 15)
;

INSERT INTO OPERADORAS VALUES
    ('Vodafone', 'Rojo', 99, 540, 'www.vodafone.com'),
    ('Movistar', 'Azul', 98, 610, 'www.movistar.com')
;

INSERT INTO TARIFAS VALUES
    ('Koala', 'Vodafone', 4, 'GB', 200, 'Si'),
    ('Ardilla', 'Vodafone', 3, 'GB', 300, 'Si')
;
