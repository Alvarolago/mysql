DROP DATABASE IF EXISTS Moviles;
CREATE DATABASE Moviles;

USE Moviles;

<<<<<<< HEAD
CREATE TABLE CLIENTES(
    DNI CHAR(9),
    nombre VARCHAR(20),
    apellidos VARCHAR(20),
    telefono CHAR(9),
    email VARCHAR(30)UNIQUE,
    CONSTRAINT PK_CLIENTES PRIMARY KEY(DNI));
DESC CLIENTES;

CREATE TABLE TIENDAS(
    nombre VARCHAR(20),
    provincia VARCHAR(20),
    localidad VARCHAR(20),
    direccion VARCHAR(50),
    teléfono CHAR(9),
    diaapertura ENUM('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'),
    diacierre ENUM('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'),
    horaAperura TINYINT UNSIGNED,
    horaCierre TINYINT UNSIGNED,
    PRIMARY KEY (nombre));
DESC TIENDAS;

CREATE TABLE OPERADORAS(
    nombre VARCHAR(20),
    colorlogo ENUM('Rojo','Naranja','Azul'),
    porcentajeCobertura TINYINT UNSIGNED COMMENT '%',
    frecuenciaGSM SMALLINT UNSIGNED,
    paginaWeb VARCHAR(100),
    CONSTRAINT PK_OPERADORAS PRIMARY KEY(nombre));
DESC OPERADORAS;

CREATE TABLE TARIFAS(
    nombre VARCHAR(50),
    nombreOperadoras VARCHAR(20),
    tamañodatos TINYINT UNSIGNED COMMENT 'Unidades en GB',
    tipoDatos ENUM('MB','GB','TB'),
    minutosGratis SMALLINT UNSIGNED,
    SMSgratis ENUM('SI','NO'),
    CONSTRAINT PK_TARIFAS PRIMARY KEY(nombre),
    CONSTRAINT PK_OPERADORAS FOREIGN KEY(nombreOperadoras) REFERENCES OPERADORAS(nombre));
DESC TARIFAS;



CREATE TABLE MOVILES(
    marca VARCHAR(20),
    modelo VARCHAR(30),
    descripcion VARCHAR(300),
    SO ENUM('iOs','Android','Windows Phone'),
    RAM TINYINT UNSIGNED,
    pulgadaspantalla COMMENT 'Unidades en pulgadas',
    camaraMpx TINYINT,
    PRIMARY KEY(marca, modelo));
=======
CREATE TABLE CLIENTES(DNI CHAR(9), nombre VARCHAR(20), apellidos VARCHAR(20), telefono CHAR(9), email VARCHAR(30)UNIQUE, CONSTRAINT PK_CLIENTES PRIMARY KEY(DNI));
DESC CLIENTES;

CREATE TABLE TIENDAS (nombre VARCHAR(20), provincia VARCHAR(20), localidad VARCHAR(20), direccion VARCHAR(50), teléfono CHAR(9), diaapertura ENUM('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'), diacierre ENUM('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'), horaAperura TINYINT UNSIGNED, horaCierre TINYINT UNSIGNED, PRIMARY KEY (nombre));
DESC TIENDAS;

CREATE TABLE OPERADORAS (nombre VARCHAR(20), colorlogo ENUM('Rojo','Naranja','Azul'), porcentajeCobertura TINYINT UNSIGNED COMMENT '%', frecuenciaGSM SMALLINT UNSIGNED, paginaWeb VARCHAR(100),CONSTRAINT PK_OPERADORAS PRIMARY KEY(nombre));
DESC OPERADORAS;

CREATE TABLE TARIFAS(nombre VARCHAR(50), nombreOperadoras VARCHAR(20), tamañodatos TINYINT UNSIGNED COMMENT 'Unidades en GB', tipoDatos ENUM('MB','GB','TB'), minutosGratis SMALLINT UNSIGNED, SMSgratis ENUM('SI','NO'),CONSTRAINT PK_TARIFAS PRIMARY KEY(nombre), CONSTRAINT PK_OPERADORAS FOREIGN KEY(nombreOperadoras) REFERENCES OPERADORAS(nombre));
DESC TARIFAS;


/*
CREATE TABLE MOVILES (marca VARCHAR(20), modelo VARCHAR(30) ,descripcion VARCHAR(300), SO ENUM('iOs','Android','Windows Phone'), RAM TINYINT UNSIGNED, pulgadaspantalla COMMENT 'Unidades en pulgadas', camaraMpx TINYINT, PRIMARY KEY(marca, modelo));
>>>>>>> faa8d3e1280926eb1f036b48b88d45d467aeead8
DESC MOVILES;

CREATE TABLE MOVILES_LIBRE (
    marcaMoviles VARCHAR(20),
    modeloMoviles VARCHAR(10),
    precio TINYINT,
    PRIMARY KEY (modeloMoviles),
    FOREIGN KEY (modeloMoviles , marcaMoviles) REFERENCES MOVILES(modelo, marca));
DESC MOVILES_LIBRE;

CREATE TABLE MOVIL_CONTRATO (
    marcaMoviles VARCHAR(20),
    modeloMoviles VARCHAR(10),
    nombreOperadoras VARCHAR(20),
    precio DECIMAL(6,2),
    CONSTRAINT FK_CONTRATO_MOVILES FOREIGN KEY(marcaMoviles, modeloMoviles) REFERENCES Moviles(marca,modelo),
    CONSTRAINT FK_CONTRATO_OPERADORAS FOREIGN KEY(nombreOperadoras) REFERENCES OPERADORAS(nombre));
DESC MOVIL_CONTRATO;

CREATE TABLE OFERTAS (
    nombreOperadorasTarifas VARCHAR(20),
    nombreTarifas VARCHAR(50),
    marcaMovilContrato VARCHAR(20),
    modeloMovilContrato VARCHAR(30),
    PRIMARY KEY(nombreTarifas, marcaMovilContrato , modeloMovilContrato),
    FOREIGN KEY(nombreTarifas,nombreOperadorasTarifas ) REFERENCES TARIFAS(nombre, nombreOperadoras),
    FOREIGN KEY (marcaMovilContrato , modeloMovilContrato) REFERENCES MOVIL_CONTRATO( marcaMoviles, modeloMoviles));
DESC OFERTAS;

CREATE TABLE COMPRAS (
    DNICliente INT,
    nombreTienda VARCHAR(20),
    marcaMovilesLibre VARCHAR(20),
    modeloMovilesLibre VARCHAR(20),
    dia DATE,
    PRIMARY KEY(DNICliente,nombreTienda,marcaMovilesLibre,modeloMovilesLibre),
    FOREIGN KEY(DNI) REFERENCES CLIENTES(DNI),
    FOREIGN KEY(nombreTienda) REFERENCES TIENDAS(nombre),
    FOREIGN KEY(marcaMovilesLibre,modeloMovilesLibre) REFERENCES MOVILES_LIBRE(marcaMoviles, modeloMoviles));
DESC COMPRAS;

CREATE TABLE CONTRATOS (DNICliente TINYINT, nombreTienda VARCHAR(20), nombreOperadorasTarifasOfertas VARCHAR(20), marcaMovilesOfertas VARCHAR(20), modeloMovilesOfertas VARCHAR(20), dia DATE, PRIMARY KEY(DINCliente, nombreTienda, nombreOperadorasTarifasOfertas, marcaMovilesOfertas, modeloMovilesOfertas), FOREIGN KEY(DNICliente) REFERENCES CLIENTES(DNI), FOREIGN KEY(nombreOperadorasTarifasOfertas, nombreOperadorasTarifasOfertas, modeloMovilesOfertas) REFERENCES OFERTAS(nombreOperadorasTarifas, nombreOperadorasTarifas, modeloMoviles);
DESC CONTRATOS;
*/
