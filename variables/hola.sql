DROP PROCEDURE IF EXIST holaProc;
DROP FUNCTION IF EXIST holaFunc;

delimiter //

CREATE PROCEDURE holaProc(IN nombre VARCHAR(20))
BEGIN
SELECT (CONCAT('Hola',nombre)) AS 'Salida Terminal';
END//

delimiter ;

CALL holaProc('Alvaro');

CREATE FUNCTION holaFunc ()
RETURNS VARCHAR(30)           /*RETURNS Retorna el tipo de dato.*/
RETURN ('Hola');

SELECT holaFunc();
