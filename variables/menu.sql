DROP PROCEDURE IF EXISTS crearInfo;

USE Jardineria;

DELIMITER |

CREATE PROCEDURE crearInfo()


BEGIN

    DECLARE numClientes, numEmpleados, numPedidos INT;

    DROP TABLE IF EXISTS INFO;
    CREATE TABLE INFO (NumClientes INT, NumEmpleados INT, NumPedidos INT);

    SET numClientes = (SELECT COUNT(*) FROM Clientes);
    SET numEmpleados = (SELECT COUNT(*) FROM Empleados);
    SET numPedidos = (SELECT COUNT(*) FROM Pedidos);

    INSERT INTO INFO VALUES (numClientes,numEmpleados,numPedidos);
END

|
DELIMITER ;

/*MIRAR EL TRIGER*/
