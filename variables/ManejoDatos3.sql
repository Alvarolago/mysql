DROP PROCEDURE IF EXISTS Ejercicio3;

DELIMITER |

CREATE PROCEDURE Ejercicio3()

BEGIN
	DECLARE CodCliente INT;

	SET CodCliente = (SELECT CodigoCliente FROM Clientes WHERE LimiteCredito = (SELECT MIN(LimiteCredito) FROM Clientes));
	DELETE FROM Pagos WHERE CodigoCliente = CodCliente;

END;
|

DELIMITER ;
