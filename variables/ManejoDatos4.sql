DROP PROCEDURE IF EXISTS Ejercicio4;

DELIMITER |

CREATE PROCEDURE Ejercicio4()

BEGIN
	DECLARE CodPedido INT;

	SET CodPedido = (SELECT CodigoPedido FROM DetallePedidos WHERE CodigoProducto LIKE 'OR-179' GROUP BY CodigoPedido LIMIT 1);

	UPDATE Clientes SET LimiteCredito = 0 WHERE CodigoCliente = CodPedido;

END
|

DELIMITER ;
