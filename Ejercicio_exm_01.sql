DROP DATABASE IF EXISTS Ejercicio_exm_01;
CREATE DATABASE Ejercicio_exm_01;

USE Ejercicio_exm_01;


CREATE TABLE PERSONA(nombre VARCHAR(20), apellidos VARCHAR(40),trabajo VARCHAR (30), PRIMARY KEY(nombre, apellidos));


CREATE TABLE SITUACIONES(hora TINYINT UNSIGNED, lugar VARCHAR(20),nombrePERSONA VARCHAR(20), apellidosPERSONA VARCHAR(20), vestuario VARCHAR(20), mercancia(20), PRIMARY KEY (hora), FOREIGN KEY(nombrePERSONA, apellidosPERSONA) REFERENCES PERSONA(nombre, apellidos));



CREATE TABLE OBJETO(nombre VARCHAR(20), tamaño ENUM('pequeño','mediano','grande'), nombreOBJETOcontenedor VARCHAR(20), PRIMARY KEY(nombre), FOREIGN KEY (nombreOBJETOcontenedor));

CREATE TABLE lleva (horaSITUACION TINYINT UNSIGNED, nombreOBJETO VARCHAR(20), FOREIGN KEY(nombreOBJETO) REFERENCES OBJETO(nombre));

