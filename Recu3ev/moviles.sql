DROP DATABASE IF EXISTS moviles;
CREATE DATABASE moviles;
USE moviles

CREATE TABLE Clientes(
	DNI CHAR(9),
	Nombre VARCHAR(20),
	Apellidos VARCHAR(20),
	Telefono CHAR(9),
	Email VARCHAR(20),
CONSTRAINT PK_Clientes PRIMARY KEY (DNI)
);
DESC Clientes;

CREATE TABLE Tiendas(
	Nombre VARCHAR(30),
	Provincia VARCHAR(20),
	Localidad VARCHAR(20),
	Direccion VARCHAR(20),
	Telefono TINYINT,
	DiaApertura ENUM('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'),
	DiaCierre ENUM('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'),
	HoraApertura TINYINT UNSIGNED,
        HoraCierre TINYINT UNSIGNED,
CONSTRAINT PK_Tiendas PRIMARY KEY (Nombre)	
);
DESC Tiendas;

CREATE TABLE Operadoras(
	Nombre VARCHAR(20),
	ColorLogo VARCHAR(20),
	PorcentajeCobertura TINYINT UNSIGNED COMMENT '%',
	FrecuenciaGSM FLOAT,
	PagWeb VARCHAR(50),
CONSTRAINT PK_Operadoras PRIMARY KEY(Nombre)
);
DESC Operadoras;

CREATE TABLE Tarifas(
	Nombre VARCHAR(20),
	Nombre_Operadoras VARCHAR(20),
	TamañoDatos TINYINT,
	TipoDatos VARCHAR(20),
	MinutosGratis TINYINT UNSIGNED,
	SMSGratis TINYINT UNSIGNED,
CONSTRAINT PK_Tarifas PRIMARY KEY(Nombre),
CONSTRAINT FK_Tarifas FOREIGN KEY(Nombre_Operadoras) REFERENCES Operadoras(Nombre)
);
DESC Tarifas;

CREATE TABLE Moviles(
	Marca VARCHAR(20),
	Modelo VARCHAR(20),
	Descripcion VARCHAR(100),
	SO ENUM('Android','Microsoft','Apple'),
	RAM TINYINT COMMENT 'Mb',
	PulgadasPantalla TINYINT COMMENT 'Pulgadas',
	CamaraMpx TINYINT COMMENT 'Mpx',
CONSTRAINT PK_Moviles PRIMARY KEY(Marca,Modelo)
);

CREATE TABLE Movil_Libre(
       Marca_Moviles VARCHAR(20),
       Modelo_Moviles VARCHAR(20),
       Precio TINYINT COMMENT '$',
CONSTRAINT FK_Movil_Libre FOREIGN KEY(Marca_Moviles, Modelo_Moviles) REFERENCES Moviles(Marca, Modelo)
);

CREATE TABLE Movil_Contrato(
	Marca_Moviles VARCHAR(20),
	Modelo_Moviles VARCHAR(20),
	Nombre_Operadoras VARCHAR(20),
	Precio TINYINT,
CONSTRAINT FK_Moviles_Contrato FOREIGN KEY(Marca_Moviles,Modelo_Moviles) REFERENCES Moviles(Marca,Modelo),
CONSTRAINT FK_Moviles_Operadoras FOREIGN KEY(Nombre_Operadoras) REFERENCES Operadoras(Nombre)
);











