DROP DATABASE IF EXISTS METEO;
CREATE DATABASE METEO;
USE METEO

CREATE TABLE Ciudad (
	Codigo VARCHAR(9),
	Nombre VARCHAR(20),
CONSTRAINT PK_Ciudad PRIMARY KEY(Codigo)
);

CREATE TABLE Temperatura(
	Dia DATE,
	Codigo_Ciudad VARCHAR(9),
	Grados TINYINT COMMENT 'º',
CONSTRAINT FK_Temperatura FOREIGN KEY(Codigo_Ciudad) REFERENCES Ciudad(Codigo)
);

CREATE TABLE Humedad(
	Dia DATE,
	Codigo_Ciudad VARCHAR(9),
	Porcentaje FLOAT(5,2) COMMENT '%',
CONSTRAINT FK_Humedad FOREIGN KEY(Codigo_Ciudad) REFERENCES Ciudad(Codigo)
);

CREATE TABLE Informe(
	ID TINYINT,
	Dia DATE,
	Codigo_Ciudad VARCHAR(9),
CONSTRAINT FK_Informe FOREIGN KEY(Codigo_Ciudad) REFERENCES Ciudad(Codigo)
);

